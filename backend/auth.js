import dotenv from "dotenv";
dotenv.config();
import jwt from "jsonwebtoken";

export function authenticateToken(req, res, next) {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];

	if (token == null) return res.sendStatus(401);

	jwt.verify(token, process.env.JWT_SECRET, (err, korisnik) => {
		console.log(err);

		if (err) return res.sendStatus(403);

		req.korisnik = korisnik;

		next();
	});
}

export function generateAccessToken(id) {
	return jwt.sign(id, process.env.JWT_SECRET, { expiresIn: "1d" });
}
