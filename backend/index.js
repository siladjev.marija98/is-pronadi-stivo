import bodyParser from "body-parser";
import express from "express";
const app = express();
import mysql from "mysql";
import cors from "cors";
import { authenticateToken, generateAccessToken } from "./auth.js";
import bcrypt from "bcrypt";
import { dbConfig } from "./dbConfig.js";
const saltRounds = 10;

app.use(
  cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    credentials: true,
  })
);
app.use(express.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));

const db = mysql.createPool(dbConfig); // konekcija prema mysql bazi i prosljedjivanje dbConfig

// dohvaćanje Knjiga iz baze
app.get("/knjiga", authenticateToken, (req, res) => {
  const sqlSelect = "select * from Knjiga where Status=1;";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

app.get("/mojeknjige", authenticateToken, (req, res) => {
  let korisnik = req.korisnik;
  const sqlSelect = "select * from Knjiga where ID_korisnika=?;";
  db.query(sqlSelect, korisnik.id, (err, result) => {
    res.send(result);
  });
});

// status knjige aktivan/neaktivan
app.post("/status", authenticateToken, (req, res) => {
  let id = req.body.ID_knjige;
  let status = req.body.status ? 1 : 0;
  const sqlUpdate = "update Knjiga set Status=? where ID_knjige=?;";
  db.query(sqlUpdate, [status, id], (err, result) => {
    if (err) {
      console.log(err);
    }
    if (result) {
      res.send("Spremljene promjene!");
    }
  });
});

// dohvaćanje detalja knjige iz baze
app.get("/detalji/:id", authenticateToken, (req, res) => {
  const id = req.params.id;
  const sqlSelect =
    "select k.ID_knjige, k.Naziv, k.Autor, z.Naziv as Zanr, k.Cijena, k.Opis, k.Status, k.ID_knjige, k.ID_korisnika, k.Datum_objave, concat(u.Ime,' ',u.Prezime) as ImePrezime, u.Kontakt, u.Email from Knjiga as k left join Zanr as z on z.ID_zanra=k.Zanr left join Korisnik as u on k.ID_korisnika=u.ID_korisnika where k.ID_knjige=?;";
  db.query(sqlSelect, id, (err, result) => {
    res.send(result[0]);
  });
});
// concat za spremanje imena i prezimena u jedno polje

// dohvaćanje Slika iz baze
app.get("/slika/:id", authenticateToken, (req, res) => {
  const id = req.params.id;
  const sqlSelect = "select ID_slike,Slika from Slika where ID_knjige=?;";
  db.query(sqlSelect, id, (err, result) => {
    res.send(result);
  });
});

// dohvaćanje Korisnika iz baze
app.get("/korisnik", authenticateToken, (req, res) => {
  let id = req.korisnik.id;
  const sqlSelect =
    "select Ime,Prezime,Adresa,Kontakt,Email from Korisnik where ID_korisnika=?;";
  db.query(sqlSelect, id, (err, result) => {
    res.send(result[0]);
  });
});

// dohvaćanje Favorita iz baze
app.get("/favoriti", authenticateToken, (req, res) => {
  const korisnik = req.korisnik.id;
  const sqlSelect =
    "select k.ID_knjige, k.Naziv, k.Autor, k.Status from Favoriti as f left join Knjiga as k on f.ID_knjige=k.ID_knjige where f.ID_korisnika=?;";
  db.query(sqlSelect, korisnik, (err, result) => {
    res.send(result);
  });
});

// provjera da li je knjiga dodana u favorite
app.get("/provjerafavorita/:id", authenticateToken, (req, res) => {
  const korisnik = req.korisnik.id;
  const knjiga = req.params.id;
  const sqlSelect =
    "select k.ID_knjige from Favoriti as f left join Knjiga as k on f.ID_knjige=k.ID_knjige where f.ID_korisnika=? and f.ID_knjige=?;";
  db.query(sqlSelect, [korisnik, knjiga], (err, result) => {
    res.send(result);
  });
});

// dodavanje favorita
app.post("/dodajfavorita", authenticateToken, (req, res) => {
  const korisnik = req.korisnik.id;
  const knjiga = req.body.ID_knjige;
  const sqlInsert = "insert into Favoriti(ID_korisnika,ID_knjige) Values(?,?)";
  db.query(sqlInsert, [korisnik, knjiga], (err, result) => {
    res.send(result);
  });
});

// brisanje favorita (uklanjanje, odznacavanje)
app.post("/brisifavorita", authenticateToken, (req, res) => {
  const korisnik = req.korisnik.id;
  const knjiga = req.body.ID_knjige;
  const sqlInsert = "delete from Favoriti where ID_korisnika=? and ID_knjige=?";
  db.query(sqlInsert, [korisnik, knjiga], (err, result) => {
    res.send(result);
  });
});

// dohvaćanje zanrova iz baze
app.get("/zanrovi", authenticateToken, (req, res) => {
  const sqlSelect = "select * from Zanr;";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

// unos knjige
app.post("/unosKnjige", authenticateToken, (req, res) => {
  const ID_korisnika = req.korisnik.id;
  const naziv = req.body.naziv;
  const autor = req.body.autor;
  const zanr = req.body.zanr;
  const cijena = req.body.cijena;
  const opis = req.body.opis;
  const slikeBase64 = req.body.slike;
  const lat = req.body.lat;
  const lng = req.body.lng;
  const datum = req.body.datum;

  if (naziv && autor && zanr && opis && slikeBase64.length > 0 && lat && lng) {
    const sqlInsert =
      "insert into Knjiga(Naziv,Autor,Zanr,Cijena,Opis,Lat,Lng,ID_korisnika,Datum_objave) Values(?,?,?,?,?,?,?,?,?)";
    const sqlInsertImg = "insert into Slika(Slika,ID_knjige) values ?";
    db.query(
      sqlInsert,
      [naziv, autor, zanr, cijena, opis, lat, lng, ID_korisnika, datum],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          let slike = [];
          slikeBase64.forEach((slika) => {
            slike.push([base64ToBuffer(slika.url), result.insertId]);
          });
          db.query(sqlInsertImg, [slike], (err, resultImg) => {
            console.log(err);
          });
          res.json({ message: "Podaci su uspješno spremljeni!" });
        }
      }
    );
  } else {
    res.json({ message: "Molimo unesite sve potrebne podatke" });
  }
});

const base64ToBuffer = (slika) => {
  return Buffer.from(slika, "base64");
};

//prijava
app.post("/prijava", (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  if (email && password) {
    db.query(
      "select * from Korisnik WHERE Email = ?",
      [email],
      (err, results) => {
        if (err) throw err;
        if (results.length > 0) {
          if (bcrypt.compareSync(password, results[0].Lozinka)) {
            let id = results[0].ID_korisnika;
            const token = generateAccessToken({ id });
            res.json({ token });
          } else {
            res.json({ message: "Pogrešan email ili loznika!" });
          }
        } else {
          res.json({ message: "Pogrešan email ili loznika!" });
        }
      }
    );
  } else {
    res.json({ message: "Molimo unesite email i loziku!" });
  }
});

//registracija korisnika
app.post("/registracija", (req, res) => {
  let ime = req.body.ime;
  let prezime = req.body.prezime;
  let adresa = req.body.adresa;
  let kontakt = req.body.kontakt;
  let email = req.body.email;
  let password = req.body.lozinka;
  const sqlInsert =
    "insert into Korisnik(Ime,Prezime,Adresa,Kontakt,Email,Lozinka) Values(?,?,?,?,?,?)";

  if (ime && prezime && adresa && kontakt && email && password) {
    db.query(
      "select * from Korisnik WHERE Email = ?",
      [email],
      (err, results) => {
        if (err) throw err;
        if (results.length > 0) {
          res.json({
            message: "Korisnički račun s ovom email adresom već postoji!",
          });
        } else {
          let hash = bcrypt.hashSync(password, saltRounds);
          db.query(
            sqlInsert,
            [ime, prezime, adresa, kontakt, email, hash],
            (err, result) => {
              if (err) throw err;
              if (result) {
                res.json({
                  message:
                    "Uspješna registracija! Prijavite se da bi mogli koristiti aplikaciju",
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({ message: "Molimo popunite sve potrebne podatke!" });
  }
});

app.listen(3001, () => {
  console.log("port 3001");
});
