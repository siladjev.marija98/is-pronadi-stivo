describe("prijava", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/knjige");
  });

  it("prikaz mape", () => {
    cy.get("#email").clear();
    cy.get("#email").type("m@hr.hr");
    cy.get("#password").clear();
    cy.get("#password").type("Marija{enter}");
    cy.get(".MuiButton-root").click();
    cy.get(".mapboxgl-canvas").click();
    /* ==== Generated with Cypress Studio ==== */
    /* ==== End Cypress Studio ==== */
  });

  /* ==== Test Created with Cypress Studio ==== */
  it("oznaka favoriti", function () {
    /* ==== Generated with Cypress Studio ==== */
    cy.get("#email").clear();
    cy.get("#email").type("m@hr.hr");
    cy.get("#password").clear();
    cy.get("#password").type("Marija{enter}");
    cy.get(".MuiButton-root").click();
    cy.get(
      '[style="transform: translate(354px, 168px) translate(-50%, -100%) rotateX(0deg) rotateZ(0deg) translate(0px, 0px);"] > [data-testid="LocationOnIcon"]'
    ).click();
    cy.get(".MuiCardActions-root > .MuiButton-textPrimary").click();
    cy.get('.MuiIconButton-root > [data-testid="FavoriteIcon"]').click();
    cy.get('[data-testid="FavoriteBorderIcon"]').click();
    cy.get('[data-testid="MenuBookIcon"]').click();
    cy.get('[data-testid="FavoriteIcon"]').click();
    cy.get('[data-testid="PersonIcon"] > path').click();
    /* ==== End Cypress Studio ==== */
  });

  /* ==== Test Created with Cypress Studio ==== */
  it("dodavanje nove knjige", function () {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#email').clear();
    cy.get('#email').type('m@hr.hr');
    cy.get('#password').clear();
    cy.get('#password').type('Marija');
    cy.get('.MuiButton-root').click();
    cy.get('.knjigeContainer > .MuiButtonBase-root').click();
    cy.get('#naziv').clear();
    cy.get('#naziv').type('Harry Potter i zatočenik Azkabana');
    cy.get('#autor').clear();
    cy.get('#autor').type('J.K. Rowling');
    cy.get('#cijena').clear();
    cy.get('#cijena').type('50');
    cy.get('#opis').clear();
    cy.get('#opis').type('Nova knjiga');
    cy.get('#adresa').clear();
    cy.get('#adresa').type('Karlobag');
    cy.get('#adresa-option-0').click();
    cy.get('.imgUploadLabel > .MuiButton-root').click();
    cy.get('#raised-button-file').click();
    cy.get('.imgUploadLabel > .MuiButton-root').click();
    cy.get('#raised-button-file').click();
    cy.get('.MuiButton-contained').click();
    /* ==== End Cypress Studio ==== */
  });
});
