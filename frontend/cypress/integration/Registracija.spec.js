describe("reistracija", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/registracija");
  });
  it("uspjesna registracija", () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#ime').clear();
    cy.get('#ime').type('Gordan');
    cy.get('#prezime').clear();
    cy.get('#prezime').type('Kinkela');
    cy.get('#adresa').clear();
    cy.get('#adresa').type('Mucici');
    cy.get('#kontakt').clear();
    cy.get('#kontakt').type('0956666665');
    cy.get('#email').clear();
    cy.get('#email').type('kinkela@gmail.com');
    cy.get('#password').clear();
    cy.get('#password').type('Test1234');
    cy.get('.MuiButton-root').click();
    cy.get('#register-form > .MuiBox-root > .MuiTypography-root').click();
    cy.get('#email').clear();
    cy.get('#email').type('kinkela@gmail.com');
    cy.get('#password').clear();
    cy.get('#password').type('Test1234{enter}');
    cy.get('.MuiButton-root').click();
    /* ==== End Cypress Studio ==== */
  });
});
