describe("prijava", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/prijava");
  });

  it("uspjesna prijava", () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get("#email").clear();
    cy.get("#email").type("m@hr.hr");
    cy.get("#password").clear();
    cy.get("#password").type("Marija");
    cy.get(".MuiButton-root").click();
    cy.wait(5000);
    //console.log(localStorage.getItem("token"));
    //expect(localStorage.getItem("token")).to.not.equal(null);
    /* ==== End Cypress Studio ==== */
  });

  it("pogresan email", function () {
    /* ==== Generated with Cypress Studio ==== */
    cy.get("#email").clear();
    cy.get("#email").type("test@gmail.com");
    cy.get("#password").clear();
    cy.get("#password").type("Test1234");
    cy.get(".MuiButton-root").click();
    cy.get(".errorText").contains("Pogrešan email ili loznika!");
    /* ==== End Cypress Studio ==== */
  });

  /* ==== Test Created with Cypress Studio ==== */
  it("pogresna lozinka", function () {
    /* ==== Generated with Cypress Studio ==== */
    cy.get("#email").clear();
    cy.get("#email").type("m@hr.hr");
    cy.get("#password").clear();
    cy.get("#password").type("Test");
    cy.get(".MuiButton-root").click();
    /* ==== End Cypress Studio ==== */
  });
});
