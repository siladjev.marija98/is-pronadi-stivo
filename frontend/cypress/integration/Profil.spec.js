describe("prijava", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/profil");
  });

  it("aktivna knjiga u neaktivna", () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get("#email").clear();
    cy.get("#email").type("m@hr.hr");
    cy.get("#password").clear();
    cy.get("#password").type("Marija{enter}");
    cy.get(".MuiButton-root").click();
    cy.get('[data-testid="PersonIcon"] > path').click();
    cy.get(
      ':nth-child(2) > .MuiPaper-root > .MuiGrid-container > .MuiGrid-grid-xs-7 > .css-j7qwjs > .MuiBox-root > .MuiButtonBase-root > [data-testid="InfoIcon"] > path'
    ).click();
    cy.get(".MuiSwitch-input").uncheck();
    cy.get('[data-testid="PersonIcon"]').click();
    /* ==== End Cypress Studio ==== */
  });

  /* ==== Test Created with Cypress Studio ==== */
  it('odjava', function() {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#email').clear();
    cy.get('#email').type('m@hr.hr');
    cy.get('#password').clear();
    cy.get('#password').type('Marija');
    cy.get('.MuiButton-root').click();
    cy.get('[data-testid="PersonIcon"] > path').click();
    cy.get('[data-testid="LogoutIcon"] > path').click();
    /* ==== End Cypress Studio ==== */
  });
});
