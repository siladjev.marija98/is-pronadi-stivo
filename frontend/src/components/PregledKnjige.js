import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  MobileStepper,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Buffer } from "buffer";
import CloseIcon from "@mui/icons-material/Close";
import InfoIcon from "@mui/icons-material/Info";

import { useNavigate } from "react-router-dom";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";

import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import { v4 as uuidv4 } from "uuid";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const PregledKnjige = ({ knjiga, closeModal }) => {
  const [slike, setSlike] = useState([]);
  const navigate = useNavigate();
  const [activeStep, setActiveStep] = useState(0);

  useEffect(() => {
    fetch(process.env.REACT_APP_API + `slika/${knjiga.ID_knjige}`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        let arraySlika = [];
        data.forEach((slika) => {
          const buffer = slika.Slika.data;
          const b64string = new Buffer(buffer).toString("base64");
          const base64 = b64string.split("dataimage/jpegbase64");
          arraySlika.push("data:image/jpeg;base64," + base64[1]);
        });
        setSlike(arraySlika);
      });
  }, [knjiga]);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Card sx={{ width: "100%", maxWidth: "600px" }}>
      <CardContent
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        {slike.length > 0 && (
          <AutoPlaySwipeableViews
            index={activeStep}
            onChangeIndex={handleStepChange}
            enableMouseEvents
            interval={5000}
          >
            {slike.map((slika, index) => (
              <CardMedia
                key={uuidv4()}
                component="img"
                height="250"
                width="100%"
                image={slika}
                alt={knjiga.Naziv}
                sx={{ objectFit: "contain" }}
              />
            ))}
          </AutoPlaySwipeableViews>
        )}
        {slike.length > 0 && (
          <MobileStepper
            steps={slike.length}
            position="static"
            activeStep={activeStep}
            nextButton={
              <Button
                size="small"
                onClick={handleNext}
                disabled={activeStep === slike.length - 1}
              >
                <KeyboardArrowRight />
              </Button>
            }
            backButton={
              <Button
                size="small"
                onClick={handleBack}
                disabled={activeStep === 0}
              >
                <KeyboardArrowLeft />
              </Button>
            }
          />
        )}
      </CardContent>
      <CardContent>
        <Typography gutterBottom variant="h6" component="div">
          {knjiga.Naziv} - {knjiga.Autor}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {knjiga.Opis}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          startIcon={<CloseIcon />}
          size="small"
          color="secondary"
          onClick={() => closeModal()}
        >
          Zatvori
        </Button>
        <Button
          startIcon={<InfoIcon />}
          size="small"
          color="primary"
          onClick={() => {
            navigate(`/detalji/${knjiga.ID_knjige}`);
          }}
        >
          Detalji
        </Button>
      </CardActions>
    </Card>
  );
};

export default PregledKnjige;
