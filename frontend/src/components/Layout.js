import { Box } from "@mui/material";
import { useLocation } from "react-router-dom";
import Navigacija from "./Navigacija";
import Zaglavlje from "./Zaglavlje";

const Layout = ({ children }) => {
  const { pathname } = useLocation();

  // zaglavlje i navigacija prikazuju se uvijek osim na stranicama za prijavu i registraciju
  return (
    <Box>
      {pathname !== "/" && pathname !== "/registracija" && <Zaglavlje />}
      <main style={{ paddingBottom: "5vh" }}>{children}</main>
      {pathname !== "/" && pathname !== "/registracija" && (
        <Box
          sx={{ width: "100%", position: "fixed", bottom: 0, height: "6vh" }}
        >
          <Navigacija />
        </Box>
      )}
    </Box>
  );
};

export default Layout;
