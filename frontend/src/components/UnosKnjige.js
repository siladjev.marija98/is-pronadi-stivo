import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import DeleteIcon from "@mui/icons-material/Delete";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useEffect, useMemo, useRef, useState } from "react";
import { Autocomplete, ImageList, ImageListItem, MenuItem } from "@mui/material";
import { v4 as uuidv4 } from "uuid";
import { debounce } from "lodash";
import { useLocation, useNavigate } from "react-router-dom";

const UnosKnjige = () => {
	const { search } = useLocation();
	const [message, setMessage] = useState(null);
	const [slike, setSlike] = useState([]);
	const [zanrovi, setZanrovi] = useState([]);
	const [odabraniZanr, setOdabraniZanr] = useState(1);
	const [geocodingResult, setGeocodingResult] = useState([]);
	const [selectedGeoLocation, setSelectedGeoLocation] = useState(null);
	const [validationErrors, setValidationErrors] = useState({
		naziv: null,
		autor: null,
		opis: null,
		adresa: null,
		slike: null,
	});

	const navigate = useNavigate();

	let adresa = useRef("");
	let forma = useRef();

	useEffect(() => {
		if (search) {
			let lat = Number(search.split("&")[0].replace("?", "").replace("lat=", ""));
			let lng = Number(search.split("&")[1].replace("lng=", ""));
			setSelectedGeoLocation({ value: [lng, lat] });
		}
	}, [search]);

	useEffect(() => {
		fetch(process.env.REACT_APP_API + "zanrovi", {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				setZanrovi(data);
			});
	}, []);

	const getImgURL = (img) => {
		return new Promise((res, rej) => {
			const reader = new FileReader();
			reader.onload = (e) => res(e.target.result);
			reader.onerror = (e) => rej(e);
			reader.readAsDataURL(img);
		});
	};

	const handleGeocodingSearch = () => {
		let address = adresa.current.value;
		if (address.length > 0) {
			fetch(
				`https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?country=hr&proximity=ip&types=place%2Cpostcode%2Caddress&fuzzyMatch=true&access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`
			)
				.then((response) => response.json())
				.then((data) => {
					let result = data.features.map((location) => {
						return { label: location.place_name, value: location.center };
					});

					setGeocodingResult(result);
				});
		}
	};

	const debouncedResults = useMemo(() => {
		return debounce(handleGeocodingSearch, 1000);
	}, []);

	useEffect(() => {
		return () => {
			debouncedResults.cancel();
		};
	});

	const handleAdressSelect = (option) => {
		setSelectedGeoLocation(option);
	};

	const handleZanrSelect = (e) => {
		setOdabraniZanr(e.target.value);
	};
	const handleImageUpload = async (e) => {
		let slika = e.target.files[0];
		let url = await getImgURL(slika);
		let id = uuidv4(); // za kreiranje random ID
		setSlike([...slike, { id: id, url: url }]);
	};

	const brisiSliku = (i) => {
		const noveSlike = slike.filter(function (slika, index) {
			return index !== i;
		});
		setSlike(noveSlike);
	};

	const validateData = (formData) => {
		let isValid = true;
		let errors = {
			naziv: null,
			autor: null,
			opis: null,
			adresa: null,
			slike: null,
		};
		setValidationErrors(errors);

		const { naziv, autor, opis, lat, lng, slike } = formData;

		if (!naziv && !autor && !opis && !lat && !lng) {
			errors = {
				naziv: "Unesite naziv knjige.",
				autor: "Unesite autora knjige.",
				opis: "Unesite opis knjige.",
				adresa: "Odaberite lokaciju knjige.",
				slike: "Dodajte slike knjige.",
			};
			isValid = false;
		} else {
			if (!naziv) {
				errors = {
					...errors,
					naziv: "Unesite naziv knjige.",
				};
				isValid = false;
			}
			if (!autor) {
				errors = {
					...errors,
					autor: "Unesite autora knjige.",
				};
				isValid = false;
			}
			if (!opis) {
				errors = {
					...errors,
					opis: "Unesite opis knjige.",
				};
				isValid = false;
			}
			if (!lat || !lng) {
				errors = {
					...errors,
					adresa: "Odaberite lokaciju knjige.",
				};
				isValid = false;
			}
			if (slike.length === 0) {
				errors = {
					...errors,
					slike: "Dodajte slike knjige.",
				};
				isValid = false;
			}
		}

		setValidationErrors(errors);

		return isValid;
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		const knjiga = {
			naziv: data.get("naziv"),
			autor: data.get("autor"),
			zanr: data.get("zanr"),
			cijena: data.get("cijena"),
			opis: data.get("opis"),
			lng: selectedGeoLocation ? selectedGeoLocation?.value[0] : "",
			lat: selectedGeoLocation ? selectedGeoLocation?.value[1] : "",
			slike: slike,
			datum: new Date(),
		};

		let isValid = validateData(knjiga);

		if (isValid) {
			fetch(process.env.REACT_APP_API + "unosKnjige", {
				method: "POST",
				headers: {
					"Content-type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify(knjiga),
			})
				.then((response) => response.json())
				.then((data) => {
					forma.current.reset();
					setGeocodingResult([]);
					setSlike([]);
					setSelectedGeoLocation(null);
					setMessage(data?.message);
				});
		}
	};
	return (
		<Container component="main" maxWidth="sm">
			<Box
				sx={{
					marginTop: 4,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<Typography component="h1" variant="h5">
					Objavi knjigu
				</Typography>
				<Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }} id="unos-knjige-form" ref={forma}>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<TextField
								name="naziv"
								required
								fullWidth
								id="naziv"
								label="Naziv"
								error={validationErrors.naziv ? true : false}
								helperText={validationErrors.naziv ? validationErrors.naziv : ""}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								required
								fullWidth
								id="autor"
								label="Autor"
								name="autor"
								error={validationErrors.autor ? true : false}
								helperText={validationErrors.autor ? validationErrors.autor : ""}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								select
								value={odabraniZanr}
								onChange={handleZanrSelect}
								required
								fullWidth
								id="zanr"
								label="Žanr"
								name="zanr"
							>
								{zanrovi.map((zanr) => (
									<MenuItem key={zanr.ID_zanra} value={zanr.ID_zanra}>
										{zanr.Naziv}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField fullWidth id="cijena" label="Cijena" name="cijena" />
						</Grid>
						<Grid item xs={12}>
							<TextField
								required
								fullWidth
								id="opis"
								label="Opis knjige"
								name="opis"
								error={validationErrors.opis ? true : false}
								helperText={validationErrors.opis ? validationErrors.opis : ""}
							/>
						</Grid>
						{!search && (
							<Grid item xs={12}>
								<Autocomplete
									disablePortal
									id="adresa"
									options={geocodingResult}
									getOptionLabel={(option) => (option.label ? option.label : "")}
									fullWidth
									renderInput={(params) => (
										<TextField
											{...params}
											label="Adresa"
											name="adresa"
											inputRef={adresa}
											error={validationErrors.adresa ? true : false}
											helperText={validationErrors.adresa ? validationErrors.adresa : ""}
										/>
									)}
									renderOption={(props, option) => (
										<Box component="li" sx={{ "& > img": { mr: 2, flexShrink: 0 } }} {...props}>
											{option.label}
										</Box>
									)}
									onInputChange={debouncedResults}
									onChange={(props, option) => {
										handleAdressSelect(option);
									}}
								></Autocomplete>
							</Grid>
						)}

						<Grid item xs={12}>
							<input
								accept="image/*"
								style={{ display: "none" }}
								id="raised-button-file"
								type="file"
								onChange={handleImageUpload}
								disabled={slike?.length >= 6}
							/>
							<label htmlFor="raised-button-file" className="imgUploadLabel">
								<Button
									variant="raised"
									component="span"
									disabled={slike?.length >= 6}
									className="uploadImageButton"
									sx={{ mt: 1, mb: 1, width: "100%" }}
								>
									UČITAJ SLIKE
								</Button>
							</label>
							{slike.length === 0 && validationErrors?.slike && (
								<Typography className="errorText">{validationErrors?.slike}</Typography>
							)}
						</Grid>
						{slike?.length > 0 && (
							<Grid item xs={12}>
								{/* lista slika */}
								<ImageList sx={{ width: "100%", height: "100%" }} cols={3} rowHeight={200}>
									{slike.map((slika, i) => (
										<ImageListItem key={slika.id} sx={{ position: "relative" }}>
											<DeleteIcon
												sx={{ position: "absolute", top: "5px", right: 0, color: "#d32f2f" }}
												onClick={() => {
													brisiSliku(i);
												}}
											/>
											<img src={slika.url} loading="lazy" alt={slika.id} />
										</ImageListItem>
									))}
								</ImageList>
							</Grid>
						)}
					</Grid>
					<Typography>{message}</Typography>
					<Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
						<Button
							variant="outlined"
							sx={{ mt: 3, mb: 2, mr: 1, width: "100%" }}
							onClick={() => {
								navigate("/knjige");
							}}
						>
							Odustani
						</Button>
						<Button type="submit" variant="contained" sx={{ mt: 3, mb: 2, ml: 1, width: "100%", color: "#ffffff" }}>
							Objavi knjigu
						</Button>
					</Box>
				</Box>
			</Box>
		</Container>
	);
};
export default UnosKnjige;
