import { Card, CardContent, CardMedia, Grid, IconButton, Typography } from "@mui/material";
import { Box } from "@mui/system";
import InfoIcon from "@mui/icons-material/Info";
import { useEffect, useState } from "react";
import { Buffer } from "buffer";
import { useNavigate } from "react-router-dom";

const CardKnjiga = ({ knjiga }) => {
	const [slika, setSlika] = useState(null);
	const navigate = useNavigate();

	useEffect(() => {
		fetch(process.env.REACT_APP_API + `slika/${knjiga.ID_knjige}`, {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((response) => response.json())
			.then((slika) => {
				const buffer = slika[0].Slika.data;
				const b64string = new Buffer(buffer).toString("base64");
				const base64 = b64string.split("dataimage/jpegbase64");
				setSlika("data:image/jpeg;base64," + base64[1]);
			});
	}, [knjiga]);

	return (
		<Card sx={{ display: "flex" }}>
			<Grid container>
				<Grid item xs={7}>
					<Box sx={{ display: "flex", flexDirection: "column" }}>
						<CardContent sx={{ flex: "1 0 auto" }}>
							<Typography component="div" variant="h6">
								{knjiga?.Naziv}
							</Typography>
							<Typography variant="subtitle1" color="text.secondary" component="div">
								{knjiga?.Autor}
							</Typography>
							<Typography variant="subtitle2" color="text.secondary" component="div">
								Status: {knjiga?.Status === 1 ? "Aktivan" : "Neaktivan"}
							</Typography>
						</CardContent>
						<Box sx={{ display: "flex", alignItems: "center", pl: 1, pb: 1 }}>
							<IconButton
								aria-label="previous"
								onClick={() => {
									navigate(`/detalji/${knjiga.ID_knjige}`);
								}}
							>
								<InfoIcon className="detaljiIcon" />
							</IconButton>
						</Box>
					</Box>
				</Grid>
				<Grid item xs={5} padding="5px">
					<CardMedia
						component="img"
						height="150"
						width="50"
						image={slika}
						alt={knjiga.Naziv}
						sx={{ objectFit: "contain" }}
					/>
				</Grid>
			</Grid>
		</Card>
	);
};

export default CardKnjiga;
