import { useState } from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import FavoriteIcon from "@mui/icons-material/Favorite";
import PersonIcon from "@mui/icons-material/Person";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import { useNavigate } from "react-router-dom";
// aktivan je onaj koji je value u state-u
const Navigacija = () => {
	const [value, setValue] = useState("/");
	let navigate = useNavigate();

	return (
		<BottomNavigation
			showLabels
			value={value}
			onChange={(event, newValue) => {
				setValue(newValue);
				navigate(newValue);
			}}
		>
			<BottomNavigationAction value="/knjige" label="Knjige" icon={<MenuBookIcon />} />
			<BottomNavigationAction value="/favoriti" label="Favoriti" icon={<FavoriteIcon />} />
			<BottomNavigationAction value="/profil" label="Profil" icon={<PersonIcon />} />
		</BottomNavigation>
	);
};
export default Navigacija;
