import { AppBar, Avatar, Toolbar } from "@mui/material";

const Zaglavlje = () => {
	return (
		<AppBar position="sticky" style={{ height: "8vh", zIndex: 99 }}>
			<Toolbar className="zaglavlje" sx={{ display: "flex", justifyContent: "flex-end" }}>
				<Avatar alt="Pronadji stivo" src="/icon.png" sx={{ width: 60, height: 60, marginLeft: "20px" }} />
			</Toolbar>
		</AppBar>
	);
};

export default Zaglavlje;
