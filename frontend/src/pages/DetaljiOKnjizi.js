import {
  Button,
  CardMedia,
  Container,
  Grid,
  IconButton,
  MobileStepper,
  Switch,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Buffer } from "buffer";
import { v4 as uuidv4 } from "uuid";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import jwt_decode from "jwt-decode";

import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const DetaljiOKnjizi = () => {
  const { id } = useParams();
  const [knjiga, setKnjiga] = useState(null);
  const [korisnik, setKorisnik] = useState(null);
  const [slike, setSlike] = useState([]);
  const [activeStep, setActiveStep] = useState(0);
  const [checked, setChecked] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    let token = localStorage.getItem("token");
    if (token) {
      let decoded = jwt_decode(token);
      setKorisnik(decoded.id);
    } else {
      navigate("/");
    }
  }, [navigate]);

  useEffect(() => {
    fetch(process.env.REACT_APP_API + `detalji/${id}`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        setKnjiga(data);
        if (data.Status === 1) {
          setChecked(true);
        } else {
          setChecked(false);
        }
      });
    fetch(process.env.REACT_APP_API + `slika/${id}`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        let arraySlika = [];
        data.forEach((slika) => {
          const buffer = slika.Slika.data;
          const b64string = new Buffer(buffer).toString("base64");
          const base64 = b64string.split("dataimage/jpegbase64");
          arraySlika.push("data:image/jpeg;base64," + base64[1]);
        });
        setSlike(arraySlika);
      });
  }, [id]);

  // provjera favorita
  useEffect(() => {
    fetch(process.env.REACT_APP_API + `provjerafavorita/${id}`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.length > 0) {
          setIsFavorite(true);
        } else {
          setIsFavorite(false);
        }
      });
  }, [id]);

  // za slike
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  // za dodavanje ili brisanje favorita
  const handleFavoriti = () => {
    if (isFavorite) {
      fetch(process.env.REACT_APP_API + "brisifavorita", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          ID_knjige: id,
        }),
      });
    } else {
      fetch(process.env.REACT_APP_API + "dodajfavorita", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          ID_knjige: id,
        }),
      });
    }
    setIsFavorite(!isFavorite);
  };

  const handleSwitch = () => {
    fetch(process.env.REACT_APP_API + "status", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        status: !checked,
        ID_knjige: id,
      }),
    });

    setChecked(!checked);
  };

  const formatDatuma = (d) => {
    let datum = new Date(d);
    let godina = datum.getFullYear();
    let mjesec = datum.getMonth() + 1;
    let dan = datum.getDate();

    if (dan < 10) {
      dan = "0" + dan;
    }
    if (mjesec < 10) {
      mjesec = "0" + mjesec;
    }
    return dan + "/" + mjesec + "/" + godina;
  };
  return (
    knjiga && (
      <Container component="main" maxWidth="sm" sx={{ p: 0 }}>
        <Grid container padding={2}>
          <Grid
            item
            xs={12}
            display="flex"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            position="relative"
          >
            <IconButton className="favoritiButton" onClick={handleFavoriti}>
              {isFavorite ? (
                <FavoriteIcon className="favoritiIcon" />
              ) : (
                <FavoriteBorderIcon className="favoritiIcon" />
              )}
            </IconButton>

            {slike.length > 0 && (
              <AutoPlaySwipeableViews
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
                interval={5000}
              >
                {slike.map((slika, index) => (
                  <CardMedia
                    key={uuidv4()}
                    component="img"
                    height="250"
                    width="100%"
                    image={slika}
                    alt={knjiga.Naziv}
                    sx={{ objectFit: "contain" }}
                  />
                ))}
              </AutoPlaySwipeableViews>
            )}
            {slike.length > 0 && (
              <MobileStepper
                steps={slike.length}
                position="static"
                activeStep={activeStep}
                nextButton={
                  <Button
                    size="small"
                    onClick={handleNext}
                    disabled={activeStep === slike.length - 1}
                  >
                    <KeyboardArrowRight />
                  </Button>
                }
                backButton={
                  <Button
                    size="small"
                    onClick={handleBack}
                    disabled={activeStep === 0}
                  >
                    <KeyboardArrowLeft />
                  </Button>
                }
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" margin="10px 0px 10px 0px">
              Detalji o knjizi
            </Typography>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Naziv knjige: </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Naziv}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Autor: </Typography>
            </Grid>
            <Grid item container xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Autor}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Žanr: </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Zanr}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Opis</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Opis}
              </Typography>
            </Grid>
          </Grid>
          {knjiga?.Datum_objave && (
            <Grid item container xs={12} margin="0px 0px 5px 0px">
              <Grid item xs={6}>
                <Typography variant="body1">Datum objave</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body1" fontWeight={500}>
                  {formatDatuma(knjiga?.Datum_objave)}
                </Typography>
              </Grid>
            </Grid>
          )}
          <Grid
            item
            container
            xs={12}
            margin="0px 0px 5px 0px"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={6}>
              <Typography variant="body1">Status</Typography>
            </Grid>
            <Grid item xs={6}>
              {korisnik === knjiga.ID_korisnika ? (
                <Typography variant="body1" fontWeight={500}>
                  <Switch checked={checked} onChange={handleSwitch} />
                  {checked ? "Aktivan" : "Neaktivan"}
                </Typography>
              ) : (
                <Typography variant="body1" fontWeight={500}>
                  {knjiga.Status === 1 ? "Aktivan" : "Neaktivan"}
                </Typography>
              )}
            </Grid>
          </Grid>
          {knjiga.Cijena !== 0 && (
            <Grid item container xs={12} margin="0px 0px 5px 0px">
              <Grid item xs={6}>
                <Typography variant="body1">Cijena</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body1" fontWeight={500}>
                  {knjiga?.Cijena}
                </Typography>
              </Grid>
            </Grid>
          )}
          <Grid item xs={12}>
            <Typography variant="h6" margin="20px 0px 10px 0px">
              Detalji o korisiku
            </Typography>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Ime i prezime:</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.ImePrezime}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Email: </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Email}
              </Typography>
            </Grid>
          </Grid>
          <Grid item container xs={12} margin="0px 0px 5px 0px">
            <Grid item xs={6}>
              <Typography variant="body1">Kontakt: </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight={500}>
                {knjiga?.Kontakt}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    )
  );
};

export default DetaljiOKnjizi;
