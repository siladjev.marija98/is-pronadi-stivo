import { Container, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import CardKnjiga from "../components/CardKnjiga";

const Favoriti = () => {
  const [knjige, setKnjige] = useState();

  useEffect(() => {
    fetch(process.env.REACT_APP_API + "favoriti", {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setKnjige(data);
      });
  }, []);

  return (
    <Container component="main" maxWidth="sm" sx={{ p: 0 }}>
      <Grid container padding={2}>
        <Grid item xs={12}>
          <Typography variant="h6" margin="10px 0px 10px 0px">
            Favoriti
          </Typography>
        </Grid>
        {knjige && knjige.length > 0 && (
          <Grid item xs={12}>
            {knjige.map((knjiga) => {
              return (
                <Grid item xs={12} marginBottom="10px" key={knjiga.ID_knjige}>
                  <CardKnjiga knjiga={knjiga} />
                </Grid>
              );
            })}
          </Grid>
        )}
      </Grid>
    </Container>
  );
};

export default Favoriti;
