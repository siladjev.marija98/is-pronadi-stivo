import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useRef, useState } from "react";

const Registracija = () => {
  const [message, setMessage] = useState(null);
  const [validationErrors, setValidationErrors] = useState({
    ime: null,
    prezime: null,
    adresa: null,
    kontakt: null,
    email: null,
    lozinka: null,
  });

  let forma = useRef();

  const validateData = (formData) => {
    let isValid = true;
    let errors = {
      ime: null,
      prezime: null,
      adresa: null,
      kontakt: null,
      email: null,
      lozinka: null,
    };
    setValidationErrors(errors);
    const { ime, prezime, adresa, kontakt, email, lozinka } = formData;
    if (!ime && !prezime && !adresa && !kontakt && !email && !lozinka) {
      errors = {
        ime: "Unesite ime.",
        prezime: "Unesite prezime.",
        adresa: "Unesite adresu.",
        kontakt: "Unesite kontakt.",
        email: "Unesite email adresu.",
        lozinka: "Unesite lozinku.",
      };
      isValid = false;
    } else {
      if (!ime) {
        errors = {
          ...errors,
          ime: "Unesite ime.",
        };
        isValid = false;
      }
      if (!prezime) {
        errors = {
          ...errors,
          prezime: "Unesite prezime.",
        };
        isValid = false;
      }
      if (!adresa) {
        errors = {
          ...errors,
          adresa: "Unesite adresu.",
        };
        isValid = false;
      }
      if (!kontakt) {
        errors = {
          ...errors,
          kontakt: "Unesite kontakt.",
        };
        isValid = false;
      }
      if (!email) {
        errors = {
          ...errors,
          email: "Unesite email adresu.",
        };
        isValid = false;
      } else {
        const emailReg = new RegExp(
          "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
        );
        const isValidEmail = emailReg.test(email);
        if (!isValidEmail) {
          errors = {
            ...errors,
            email:
              "Molimo unesite validnu email adresu npr. primjer@mail.com .",
          };
          isValid = false;
        }
      }
      if (!lozinka) {
        errors = {
          ...errors,
          lozinka: "Unesite lozinku.",
        };
        isValid = false;
      } else {
        const lozinkaReg = new RegExp(
          "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})"
        );
        const isPasswordValid = lozinkaReg.test(lozinka);
        if (!isPasswordValid) {
          errors = {
            ...errors,
            lozinka:
              "Lozinka mora sadržavati najmanje 8 znakova, barem jedno veliko slovo i barem jedan broj.",
          };
          isValid = false;
        }
      }
    }

    setValidationErrors(errors);
    return isValid;
  };

  const handleSubmit = (event) => {
    event.preventDefault(); // da se stranica ne refresha
    const data = new FormData(event.currentTarget);

    const korisnik = {
      ime: data.get("ime"),
      prezime: data.get("prezime"),
      adresa: data.get("adresa"),
      kontakt: data.get("kontakt"),
      email: data.get("email"),
      lozinka: data.get("password"),
    };

    let isValid = validateData(korisnik);

    if (isValid) {
      fetch(process.env.REACT_APP_API + "registracija", {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(korisnik), // prosljedjivanje podataka na backend
      })
        .then((response) => response.json())
        .then((data) => {
          forma.current.reset();
          setMessage(data?.message);
        });
    }
  };

  //prikaz podataka na stranici
  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar
          alt="Pronadji stivo"
          src="/icon.png"
          sx={{ width: 150, height: 150, marginBottom: "20px" }}
        />
        <Typography component="h1" variant="h5">
          Registriraj se pronađi štivo
        </Typography>
        <Box
          component="form"
          noValidate
          onSubmit={handleSubmit}
          sx={{ mt: 3 }}
          id="register-form"
          ref={forma}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                name="ime"
                required
                fullWidth
                id="ime"
                label="Ime"
                autoFocus
                error={validationErrors.ime ? true : false}
                helperText={validationErrors.ime ? validationErrors.ime : ""}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="prezime"
                label="Prezime"
                name="prezime"
                error={validationErrors.prezime ? true : false}
                helperText={
                  validationErrors.prezime ? validationErrors.prezime : ""
                }
              />
            </Grid>{" "}
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="adresa"
                label="Adresa"
                name="adresa"
                error={validationErrors.adresa ? true : false}
                helperText={
                  validationErrors.adresa ? validationErrors.adresa : ""
                }
              />
            </Grid>{" "}
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="kontakt"
                label="Kontakt"
                name="kontakt"
                error={validationErrors.kontakt ? true : false}
                helperText={
                  validationErrors.kontakt ? validationErrors.kontakt : ""
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email adresa"
                name="email"
                error={validationErrors.email ? true : false}
                helperText={
                  validationErrors.email ? validationErrors.email : ""
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Lozinka"
                type="password"
                id="password"
                error={validationErrors.lozinka ? true : false}
                helperText={
                  validationErrors.lozinka ? validationErrors.lozinka : ""
                }
              />
            </Grid>
          </Grid>
          <Typography
            className="errorText"
            sx={{ marginTop: "10px !important" }}
          >
            {message}
          </Typography>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2, color: "#ffffff" }}
          >
            Registriraj se
          </Button>
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Link href="/" variant="body2">
              Već imate račun? Prijavite se
            </Link>
          </Box>
        </Box>
      </Box>
    </Container>
  );
};
export default Registracija;
