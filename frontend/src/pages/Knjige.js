import {
  Box,
  Button,
  Fab,
  InputBase,
  MenuItem,
  MenuList,
  Modal,
  Paper,
  Popper,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useEffect, useMemo, useRef, useState } from "react";
import Map, {
  GeolocateControl,
  Marker,
  NavigationControl,
  Popup,
} from "react-map-gl";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AddLocationAltIcon from "@mui/icons-material/AddLocationAlt";
import PregledKnjige from "../components/PregledKnjige";
import { createSearchParams, useNavigate } from "react-router-dom";
import { Pretraga, SearchIconWrapper } from "../components/Pretraga";
import SearchIcon from "@mui/icons-material/Search";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import { debounce } from "lodash";

const Knjige = () => {
  const [viewState, setViewState] = useState({
    longitude: 16.624196,
    latitude: 44.706571,
    zoom: 5.5,
  });

  const navigate = useNavigate();

  let knjige = useRef([]);

  const [filterKnjige, setFilterKnjige] = useState([]);
  const [prikaziPopup, setPrikaziPopup] = useState(false);
  const [latLng, setLatLng] = useState(null);
  const [open, setOpen] = useState(false);
  const [odabranaKnjiga, setOdabranaKnjiga] = useState(null);
  const [zanrovi, setZanrovi] = useState([]);
  const [odabraniZanr, setOdabraniZanr] = useState(0);
  const [searchString, setSearchString] = useState("");
  const [filterOpen, setFilterOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    fetch(process.env.REACT_APP_API + "knjiga", {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        knjige.current = data;
        setFilterKnjige(data); // za pretragu i pinove
      });
  }, [navigate]);

  useEffect(() => {
    fetch(process.env.REACT_APP_API + "zanrovi", {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setZanrovi(data);
      });
  }, []);

  const onMarkerClick = (knjiga) => {
    setOdabranaKnjiga(knjiga);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFilterClick = (event) => {
    setAnchorEl(event.currentTarget);
    setFilterOpen(!filterOpen);
  };

  const handleZanrClick = (event) => {
    const listaKnjiga = knjige.current;
    setFilterOpen(!filterOpen);
    let zanr = event.target.value;
    setOdabraniZanr(event.target.value);
    if (event.target.value === 0) {
      if (searchString === "") {
        setFilterKnjige(listaKnjiga);
      } else {
        setFilterKnjige(searchFilter(listaKnjiga, searchString));
      }
    } else {
      if (searchString === "") {
        let filtriraneKnjige = zanrFilter(listaKnjiga, zanr);
        setFilterKnjige(filtriraneKnjige);
      } else {
        let filtriraneKnjigePoTekstu = searchFilter(listaKnjiga, searchString);
        let filtriraneKnjigePoZanru = zanrFilter(
          filtriraneKnjigePoTekstu,
          zanr
        );
        setFilterKnjige(filtriraneKnjigePoZanru);
      }
    }
  };

  const zanrFilter = (knjige, zanr) => {
    return knjige.filter((knjiga) => {
      if (knjiga.Zanr === zanr) {
        return knjiga;
      } else {
        return null;
      }
    });
  };

  // search = unos pretrage
  const searchFilter = (knjige, search) => {
    return knjige.filter((knjiga) => {
      if (
        knjiga.Naziv.toLowerCase().includes(search.toLowerCase()) ||
        knjiga.Autor.toLowerCase().includes(search.toLowerCase())
      ) {
        return knjiga;
      } else {
        return null;
      }
    });
  };

  const handleSearch = (e) => {
    let search = e.target.value;
    setSearchString(search);

    let lista = knjige.current;
    let filtriraneKnjige = searchFilter(lista, search);

    setFilterKnjige(filtriraneKnjige);
  };

  const debouncedResults = useMemo(() => {
    return debounce(handleSearch, 1000); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    return () => {
      debouncedResults.cancel();
    };
  });

  const onRightClick = (e) => {
    if (e.originalEvent.button === 2) {
      // lijevi klik je 1
      setLatLng(e.lngLat); // postavljanje pina na odabrani dio (kliknut)
      setPrikaziPopup(true);
    }
  };

  return (
    <Box className="knjigeContainer">
      <Pretraga
        style={{
          position: "absolute",
          top: -55,
          left: 10,
          zIndex: 100,
          width: 280,
        }}
      >
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <InputBase
          style={{ padding: "5px 0px 5px 50px", color: "#ffffff" }}
          id="search"
          placeholder="Traži"
          onChange={debouncedResults}
        />
        <FilterAltIcon
          onClick={(e) => {
            handleFilterClick(e);
          }}
        />
        <Popper
          open={filterOpen}
          anchorEl={anchorEl}
          role={undefined}
          placement="bottom-start"
          disablePortal
        >
          <Paper>
            <MenuList
              autoFocusItem={open}
              id="composition-menu"
              aria-labelledby="composition-button"
            >
              <MenuItem
                key="0"
                className={odabraniZanr === 0 ? "odabraniZanr" : ""}
                value={0}
                onClick={(e) => {
                  handleZanrClick(e);
                }}
              >
                Svi žanrovi
              </MenuItem>
              {zanrovi.length > 0 &&
                zanrovi.map((zanr) => (
                  <MenuItem
                    className={
                      odabraniZanr === zanr.ID_zanra ? "odabraniZanr" : ""
                    }
                    key={zanr.ID_zanra}
                    value={zanr.ID_zanra}
                    onClick={(e) => {
                      handleZanrClick(e);
                    }}
                  >
                    {zanr.Naziv}
                  </MenuItem>
                ))}
            </MenuList>
          </Paper>
        </Popper>
      </Pretraga>
      <Fab
        color="primary"
        aria-label="add"
        className="dodajKnjiguButton"
        onClick={() => {
          navigate("/novaknjiga");
        }}
      >
        <AddIcon style={{ color: "#ffffff" }} />
      </Fab>
      <Map
        {...viewState}
        onMove={(e) => setViewState(e.viewState)}
        style={{ width: "100vw", height: "86vh" }}
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
        onClick={() => {
          setLatLng(null);
        }}
        onMouseDown={onRightClick}
      >
        {prikaziPopup && latLng && (
          <Popup
            longitude={latLng?.lng}
            latitude={latLng?.lat}
            anchor="top"
            onClose={() => setPrikaziPopup(false)}
          >
            <Button
              startIcon={<AddIcon />}
              onClick={() => {
                navigate({
                  pathname: "/novaknjiga",
                  search: `?${createSearchParams({
                    lat: latLng?.lat,
                    lng: latLng?.lng,
                  })}`,
                });
              }}
            >
              Dodaj knjigu
            </Button>
          </Popup>
        )}
        <GeolocateControl />
        <NavigationControl showCompass={false} />
        {latLng && (
          <Marker
            key="123"
            longitude={Number(latLng.lng)}
            latitude={Number(latLng.lat)}
            anchor="bottom"
          >
            <AddLocationAltIcon />
          </Marker>
        )}

        {/* prikaz knjiga */}
        {filterKnjige?.length > 0 &&
          filterKnjige?.map((knjiga) => {
            return (
              <Marker
                key={knjiga.ID_knjige}
                longitude={Number(knjiga.Lng)}
                latitude={Number(knjiga.Lat)}
                anchor="bottom"
                onClick={() => onMarkerClick(knjiga)}
              >
                <LocationOnIcon />
              </Marker>
            );
          })}
      </Map>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: "100%",
            bgcolor: "transparent",
            p: 4,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <PregledKnjige knjiga={odabranaKnjiga} closeModal={handleClose} />
        </Box>
      </Modal>
    </Box>
  );
};

export default Knjige;
