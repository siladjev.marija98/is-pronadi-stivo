import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const Prijava = () => {
	const [message, setMessage] = useState(null);
	const navigate = useNavigate();

	useEffect(() => {
		localStorage.removeItem("token");
	}, []);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		fetch(process.env.REACT_APP_API + "prijava", {
			method: "POST",
			mode: "cors",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: data.get("email"),
				password: data.get("password"),
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.token) {
					localStorage.setItem("token", data.token);
					navigate("/knjige");
				} else {
					setMessage(data?.message);
				}
			});
	};

	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<Avatar alt="Pronadji stivo" src="/icon.png" sx={{ width: 150, height: 150, marginBottom: "20px" }} />

				<Typography component="h1" variant="h5">
					Prijavi se i pronađi svoje štivo!
				</Typography>
				<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
					<TextField
						margin="normal"
						required
						fullWidth
						id="email"
						label="Email adresa"
						name="email"
						autoComplete="email"
						autoFocus
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Lozinka"
						type="password"
						id="password"
						autoComplete="current-password"
					/>
					<Typography className="errorText">{message}</Typography>{" "}
					<Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2, color: "#ffffff" }}>
						Prijavi se
					</Button>
					<Box sx={{ display: "flex", justifyContent: "center" }}>
						<Link href="/registracija" variant="body2">
							Nemate račun? Registrirajte se!
						</Link>
					</Box>
				</Box>
			</Box>
		</Container>
	);
};
export default Prijava;
