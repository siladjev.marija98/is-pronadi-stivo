import { Container, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import LogoutIcon from "@mui/icons-material/Logout";
import CardKnjiga from "../components/CardKnjiga";

const Profil = () => {
	const [korisnik, setKorisnik] = useState(null);
	const [knjige, setKnjige] = useState([]);
	const navigate = useNavigate();

	const handleOdjava = () => {
		localStorage.removeItem("token");
		navigate("/");
	};

	useEffect(() => {
		fetch(process.env.REACT_APP_API + "korisnik", {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				setKorisnik(data);
			});
	}, []);

	useEffect(() => {
		fetch(process.env.REACT_APP_API + "mojeknjige", {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				setKnjige(data);
			});
	}, []);

	return (
		<Container component="main" maxWidth="sm" sx={{ p: 0 }}>
			<Grid container padding={2} position="relative">
				<LogoutIcon className="logoutIcon" onClick={handleOdjava} />
				<Grid item xs={12}>
					<Typography variant="h6" margin="10px 0px 10px 0px">
						Moj profil
					</Typography>
				</Grid>
				<Grid item container xs={12} margin="0px 0px 5px 0px">
					<Grid item xs={6}>
						<Typography variant="body1">Ime: </Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" fontWeight={500}>
							{korisnik?.Ime}
						</Typography>
					</Grid>
				</Grid>
				<Grid item container xs={12} margin="0px 0px 5px 0px">
					<Grid item xs={6}>
						<Typography variant="body1">Prezime: </Typography>
					</Grid>
					<Grid item container xs={6}>
						<Typography variant="body1" fontWeight={500}>
							{korisnik?.Prezime}
						</Typography>
					</Grid>
				</Grid>
				<Grid item container xs={12} margin="0px 0px 5px 0px">
					<Grid item xs={6}>
						<Typography variant="body1">Adresa: </Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" fontWeight={500}>
							{korisnik?.Adresa}
						</Typography>
					</Grid>
				</Grid>
				<Grid item container xs={12} margin="0px 0px 5px 0px">
					<Grid item xs={6}>
						<Typography variant="body1">Kontakt: </Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" fontWeight={500}>
							{korisnik?.Kontakt}
						</Typography>
					</Grid>
				</Grid>
				<Grid item container xs={12} margin="0px 0px 5px 0px">
					<Grid item xs={6}>
						<Typography variant="body1">Email: </Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" fontWeight={500}>
							{korisnik?.Email}
						</Typography>
					</Grid>
				</Grid>
				{knjige?.length > 0 && (
					<Grid item xs={12}>
						<Typography variant="h6" margin="10px 0px 10px 0px">
							Moje knjige
						</Typography>
					</Grid>
				)}
				{knjige?.length > 0 && (
					<Grid item xs={12}>
						{knjige.map((knjiga) => {
							return (
								<Grid item xs={12} marginBottom="10px" key={knjiga.ID_knjige}>
									<CardKnjiga knjiga={knjiga} />
								</Grid>
							);
						})}
					</Grid>
				)}
			</Grid>
		</Container>
	);
};

export default Profil;
