import "./App.css";
import { Route, Routes, useLocation, useNavigate } from "react-router-dom";
import Knjige from "./pages/Knjige";
import Profil from "./pages/Profil";
import Favoriti from "./pages/Favoriti";
import { Box, createTheme, ThemeProvider } from "@mui/material";
import Layout from "./components/Layout";
import { useEffect } from "react";
import Prijava from "./pages/Prijava";
import Registracija from "./pages/Registracija";
import UnosKnjige from "./components/UnosKnjige";
import DetaljiOKnjizi from "./pages/DetaljiOKnjizi";

function App() {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  useEffect(() => {
    let token = localStorage.getItem("token");
    if (!token && pathname !== "/registracija") {
      navigate("/");
    }
  }, [navigate, pathname]);

  // postavljanje boja aplikacije
  const theme = createTheme({
    palette: {
      primary: {
        main: "#f7a1a1",
      },
      secondary: {
        main: "#666666",
      },
    },
  });

  return (
    <Box>
      <ThemeProvider theme={theme}>
        <Layout>
          <Routes>
            <Route exact path="/" element={<Prijava />} />
            <Route exact path="/registracija" element={<Registracija />} />
            <Route exact path="/knjige" element={<Knjige />} />
            <Route exact path="/detalji/:id" element={<DetaljiOKnjizi />} />
            <Route exact path="/novaknjiga" element={<UnosKnjige />} />
            <Route path="/profil" element={<Profil />} />
            <Route path="/favoriti" element={<Favoriti />} />
          </Routes>
        </Layout>
      </ThemeProvider>
    </Box>
  );
}

export default App;
